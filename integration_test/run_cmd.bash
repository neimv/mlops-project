#!/bin/bash
set -x

# while true
# do
# 	echo "Press [CTRL+C] to stop.."
# 	sleep 1
# done

if [ $RUN_EXPERIMENT == "0" ]
then
    echo "running run_experiment"
    pipenv run python integration_test/run_experiment.py
elif [ $RUN_EXPERIMENT == "1" ]
then
    echo "running test_predict"
    pipenv run python integration_test/test_predict.py
else
    echo "Is not possible use this action"
fi

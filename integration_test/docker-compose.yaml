version: '3'

services:
########################################################################
# Storage Service
########################################################################
  db-service:
    image: postgres:14-alpine
    restart: always
    healthcheck:
      test: pg_isready -q -d fungis -U fungis || exit 1
      interval: 30s
      timeout: 10s
      retries: 5
    expose:
      - "5432"
    environment:
      POSTGRES_USER: fungis
      POSTGRES_PASSWORD: fungis
      POSTGRES_DB: fungis
    volumes:
      - fungis-db-volume:/var/lib/postgresql/data
    networks:
      - integration-backend

  db-orion:
    image: postgres:14-alpine
    restart: always
    healthcheck:
      test: pg_isready -q -d orion -U orion || exit 1
      interval: 30s
      timeout: 10s
      retries: 5
    expose:
      - "5432"
    environment:
      POSTGRES_USER: orion
      POSTGRES_PASSWORD: orion
      POSTGRES_DB: orion
    volumes:
      - orion-db-volume:/var/lib/postgresql/data
    networks:
      - integration-backend

  localstack:
    restart: always
    image: localstack/localstack
    expose:
      - "4566"
    environment:
      - SERVICES=s3
    volumes:
      - ./aws_pre:/docker-entrypoint-initaws.d
      - localstack-vol:/tmp/localstack
    networks:
      integration-backend:
        ipv4_address: 10.1.0.20

########################################################################
# Frontend services
########################################################################
  mlflow:
    restart: always
    image: neimv/mlflow
    # build: ./infra/mlflow
    mem_limit: 1G
    mem_reservation: 128M
    cpus: 2.0
    expose:
      - "5000"
    networks:
      - integration-frontend
      - integration-backend
    environment:
      - AWS_ACCESS_KEY_ID=test
      - AWS_SECRET_ACCESS_KEY=test
      - AWS_DEFAULT_REGION=us-west-1
      - MLFLOW_S3_ENDPOINT_URL=http://localstack:4566
    command: mlflow server
      --backend-store-uri sqlite:///mydb.sqlite
      --default-artifact-root s3://mlflow/
      --host 0.0.0.0

  prefect:
    restart: always
    image: neimv/prefect
    # build: ./infra/prefect
    mem_limit: 1G
    mem_reservation: 128M
    cpus: 2.0
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:4200/api/health"]
      interval: 30s
      timeout: 10s
      retries: 5
    environment:
      - PREFECT_ORION_DATABASE_CONNECTION_URL=postgresql+asyncpg://orion:orion@db-orion:5432/orion
      - PREFECT_ORION_DATABASE_TIMEOUT=30 # This configuration is because in my laptop does't start
      - PREFECT_ACTION=server
      - S3_ENDPOINT_URL=http://localstack:4566
      - MLFLOW_SET_TRACKING=http://mlflow:5000
      - MLFLOW_S3_ENDPOINT_URL=http://localstack:4566
      - AWS_ACCESS_KEY_ID=test
      - AWS_SECRET_ACCESS_KEY=test
      - HOST_JENKINS=jenkins:8080
      - IN_DOCKER=1
      - INTEGRATION_TEST=1
    expose:
      - "4200/udp"
      - "4200/tcp"
    networks:
      - integration-frontend
      - integration-backend
    depends_on:
      db-orion:
        condition: service_healthy

  prefect-agent:
    restart: always
    image: neimv/prefect
    # build: ./infra/prefect
    mem_limit: 4G
    mem_reservation: 128M
    cpus: 2.0
    environment:
      - PREFECT_ORION_DATABASE_CONNECTION_URL=postgresql+asyncpg://orion:orion@db-orion:5432/orion
      - PREFECT_ORION_DATABASE_TIMEOUT=30
      - PREFECT_ACTION=worker
      - PREFECT_API=http://prefect:4200/api
      - S3_ENDPOINT_URL=http://localstack:4566
      - MLFLOW_SET_TRACKING=http://mlflow:5000
      - AWS_ACCESS_KEY_ID=test
      - AWS_SECRET_ACCESS_KEY=test
      - LOCALSTACK_SERVER=localstack
      - PROJECT_PREFECT=fungus
      - HOST_JENKINS=jenkins:8080
      - IN_DOCKER=1
      - INTEGRATION_TEST=1
    networks:
      - integration-backend
    depends_on:
      prefect:
        condition: service_healthy
      localstack:
        condition: service_started

networks:
  integration-frontend:
    driver: bridge
  integration-backend:
    driver: bridge
    ipam:
      config:
        - subnet: 10.1.0.0/16
          gateway: 10.1.0.1

volumes:
  fungis-db-volume:
  localstack-vol:
  orion-db-volume:

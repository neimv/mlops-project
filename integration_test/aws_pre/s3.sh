#!/bin/sh
echo "Init localstack s3"
awslocal s3 mb s3://mlflow
awslocal s3 mb s3://prefect

awslocal s3 cp /docker-entrypoint-initaws.d/sample_submission.csv s3://mlflow/data/raw/sample_submission.csv
awslocal s3 cp /docker-entrypoint-initaws.d/test.csv s3://mlflow/data/raw/test.csv
awslocal s3 cp /docker-entrypoint-initaws.d/train.csv s3://mlflow/data/raw/train.csv

import os

import requests as reqs


def main():
    data_endpoint = [
        {
            'cap-shape': 'flat',
            'cap-surface': 'scaly',
            'cap-color': 'cinnamon',
            'bruises': 'no bruises',
            'gill-attachment': 'attached',
            'gill-spacing': 'close',
            'gill-size': 'broad',
            'gill-color': 'yellow',
            'stalk-shape': 'enlarging',
            'stalk-surface-above-ring': 'silky',
            'stalk-surface-below-ring': 'scaly',
            'stalk-color-above-ring': 'cinnamon',
            'stalk-color-below-ring': 'cinnamon',
            'veil-color': 'white',
            'spore-print-color': 'white',
            'population': 'clustered',
            'habitat': 'wood',
            'prediction': '0',
        },
        {
            'cap-shape': 'knobbed',
            'cap-surface': 'scaly',
            'cap-color': 'cinnamon',
            'bruises': 'no bruises',
            'gill-attachment': 'attached',
            'gill-spacing': 'close',
            'gill-size': 'narrow',
            'gill-color': 'buff',
            'stalk-shape': 'tapering',
            'stalk-surface-above-ring': 'smooth',
            'stalk-surface-below-ring': 'silky',
            'stalk-color-above-ring': 'white',
            'stalk-color-below-ring': 'white',
            'veil-color': 'white',
            'spore-print-color': 'white',
            'population': 'several',
            'habitat': 'leaves',
            'prediction': '1',
        },
    ]
    docker = os.getenv("ON_DOCKER", "0")
    ip_minikube = os.getenv("IP_MINIKUBE")

    if ip_minikube is None:
        url_base = (
            "localhost:8100" if docker == "0" else "ml-fungus-integration:8000"
        )
    else:
        url_base = ip_minikube + ":30088"
    url_fungs = f"http://{url_base}/api/predict"

    for data in data_endpoint:
        data_prepared = {k: v for k, v in data.items() if k != "prediction"}
        req = reqs.post(url_fungs, json=data_prepared)

        if req.status_code != 201:
            print("Failed testing endpoint")
            raise Exception(str(req.json()))


if __name__ == "__main__":
    main()

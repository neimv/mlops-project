import os
import time

import requests as reqs


def main():
    docker = os.getenv("ON_DOCKER", "0")
    ip_minikube = os.getenv("IP_MINIKUBE")
    if ip_minikube is None:
        url = (
            "http://localhost:4200" if docker == "0" else "http://prefect:4200"
        )
    else:
        url = f"http://{ip_minikube}:30010"
    flow_name = "fungus"
    deployment_name = "fungus"
    depl_id = f"/api/deployments/name/{flow_name}/{deployment_name}"
    data_run = {"state": {"type": "SCHEDULED"}}

    # Creating flow run
    deployment_id = reqs.get(f"{url}{depl_id}", timeout=20)
    if deployment_id.status_code == 200:
        print(deployment_id.json())
        deployment_id = deployment_id.json()["id"]
    else:
        raise Exception("Failed")

    print(deployment_id)
    run_flow = f"/api/deployments/{deployment_id}/create_flow_run"
    print(run_flow)
    flow_run = reqs.post(f"{url}{run_flow}", json=data_run)

    if flow_run.status_code == 201:
        id_flow = flow_run.json()["id"]
    else:
        print(flow_run)
        print(flow_run.json())
        raise Exception("Failed")

    print(id_flow)
    state_flow = f"/api/flow_runs/{id_flow}"
    while True:
        state = reqs.get(f"{url}{state_flow}")

        if state.status_code == 200:
            status_final = state.json()['state_type']
            print(status_final)

            if status_final in ("COMPLETED",):
                print(f"Finished with status: {status_final}")
                break
            if status_final in ("FAILED", "CANCELLED", "CRASHED"):
                raise Exception("Bye bye")
        else:
            raise Exception("Failed")

        print("Check every 5 minutes")
        time.sleep(5)


if __name__ == "__main__":
    main()

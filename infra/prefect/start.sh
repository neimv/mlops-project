#!/usr/bin/env bash
set -e
echo "Running prefect"

action=$PREFECT_ACTION

echo "using action: $action"

if [[ $action == "server" ]] || [[ -z $action ]]; then
    echo "Running server"
    prefect orion start --host 0.0.0.0
elif [[ $action == "worker" ]]; then
    echo "test if prefect server is running"

    while true; do
        echo "testing connection"
        echo "trying to connect: $PREFECT_API"
        echo $response
        response=$(curl -o /dev/null -s -w "%{http_code}\n" $PREFECT_API)
        sleep 2
        echo $response

        if [[ $response == "200" ]]; then
            break
        fi
    done

    echo "*************************************************************"
    echo $LOCALSTACK_SERVER
    echo "*************************************************************"

    if [[ -n $LOCALSTACK_SERVER ]]; then
        echo "*************************************************************"
        echo "Exporting ip of localstack"
        echo "*************************************************************"
        export LOCALSTACK_IP=$(dig $LOCALSTACK_SERVER +short)
    fi

    echo "Running worker"
    sleep 5
    echo "Creating new block"
    prefect block register --file remote_storage.py
    hash_prefect=`echo $RANDOM | md5sum | head -c 20; echo;`
    prefect work-queue create -t $PROJECT_PREFECT "dark-queue-$hash_prefect"
    prefect agent start -t $PROJECT_PREFECT --api $PREFECT_API # http://localhost:4200/api
else
    echo "Is not possible complete the action, is invalid"
    exit 127
fi

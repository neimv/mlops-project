from typing import Optional

import boto3
from pydantic import SecretStr
from prefect.blocks.core import Block


class LocalStackS3(Block):
    aws_access_key_id: Optional[str] = None
    aws_secret_access_key: Optional[SecretStr] = None
    region_name: Optional[str] = None
    endpoint_url: Optional[str] = None

    def get_s3_client(self):
        return boto3.client(
            service_name='s3',
            region_name=self.region_name,
            endpoint_url=self.endpoint_url,
            aws_access_key_id=self.aws_access_key_id,
            aws_secret_access_key=self.aws_secret_access_key,
        )

import os

from prefect.filesystems import S3, RemoteFileSystem
from prefect.blocks.system import JSON

ip_localstack = str(os.getenv('LOCALSTACK_IP'))
buckets = {
    'PREFECT': os.getenv("BUCKET_S3_PREFECT", "prefect"),
    'MLFLOW': os.getenv("BUCKET_S3_MLFLOW", "mlflow"),
    'NUM_TOP_EXP': os.getenv("NUM_TOP_EXP", "2"),
    'NAME_EXP_BASE': os.getenv("NAME_EXP_BASE", "Fungus"),
    'TRAIN_FILE': os.getenv("TRAIN_FILE", "train.csv"),
    'TEST_FILE': os.getenv("TEST_FILE", "test.csv"),
    'OUTPUT_TEST': os.getenv("OUTPUT_TEST", "sample_submission.csv"),
    "OUTPUT_MODELS": os.getenv("OUTPUT_MODELS", "models/"),
}
jenkins = {
    "USER": os.getenv("USER_JENKINS", "neimv"),
    "PASSWORD": os.getenv("PASS_JENKINS", "12345678"),
    "HOST": os.getenv("HOST_JENKINS", "localhost:8010"),
    "JOB_DEPLOY": os.getenv("JOB_JENKINS", "fungus_deploy"),
    "TOKEN": os.getenv("TOKEN_JENKINS", "fungus10c4l"),
}

ip_localstack = f'http://{ip_localstack}:4566'

s3RFS = RemoteFileSystem(
    basepath="s3://prefect",
    settings={
        "key": "test",
        "secret": "test",
        "client_kwargs": {"endpoint_url": ip_localstack},
    },
)
bucket = buckets["PREFECT"]
access_key = os.getenv("ACCESS_KEY")
secret_key = os.getenv("SECRET_KEY")

if access_key is None or secret_key is None:
    s3 = S3(bucket_path=bucket)
else:
    s3 = S3(
        bucket_path=bucket,
        aws_access_key_id=access_key,
        aws_secret_access_key=secret_key,
    )

json_block = JSON(value=buckets)
jenkins_block = JSON(value=jenkins)

try:
    s3RFS.save("fungusprefect")
    s3.save("fungusprefectaws")
    json_block.save("fungusconf")
    jenkins_block.save("jenkinscredentials")
except Exception as e:
    print(e)

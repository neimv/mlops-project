import io
import os

from prefect.blocks.core import Block
from localstack_s3_block_client import LocalStackS3


class S3BucketLocalStack(Block):
    bucket_name: str
    localstack: LocalStackS3

    def read(self, key: str) -> bytes:
        s3_client = self.credentials.get_s3_client()

        stream = io.BytesIO()
        s3_client.download_fileobj(
            Bucket=self.bucket_name, key=key, Fileobj=stream
        )

        stream.seek(0)
        output = stream.read()

        return output

    def write(self, key: str, data: bytes) -> None:
        s3_client = self.credentials.get_s3_client()
        stream = io.BytesIO(data)
        s3_client.upload_fileobj(stream, Bucket=self.bucket_name, Key=key)


creds = LocalStackS3(
    aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID', 'test'),
    aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY', 'test'),
    endpoint_url=os.getenv('S3_ENDPOINT_URL', 'http://s3:4566'),
    region_name=os.getenv('REGION_NAME', 'us-west-2'),
)
creds.save('localstackcreds')

my_s3_localstack = S3BucketLocalStack(
    bucket_name='prefect', localstack=LocalStackS3.load('localstackcreds')
)

my_s3_localstack.save("s3localstack")
